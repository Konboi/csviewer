package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/soh335/sliceflag"
)

func main() {
	var path, showColumns string
	var limit int
	var filters []string

	flag.StringVar(&path, "path", "", "set csv file path")
	flag.StringVar(&path, "p", "", "set csv file path")
	flag.IntVar(&limit, "limit", 0, "set limit num")
	flag.IntVar(&limit, "l", 0, "set limit num")
	flag.StringVar(&showColumns, "columns", "", "show specify columns")
	flag.StringVar(&showColumns, "c", "", "show specify columns")
	sliceflag.StringVar(flag.CommandLine, &filters, "f", []string{}, "filter")
	sliceflag.StringVar(flag.CommandLine, &filters, "filter", []string{}, "filter")

	flag.Parse()

	d, err := loadData(path)
	if err != nil {
		log.Fatal("error load data", err.Error())
	}

	c := csv.NewReader(d)

	column, err := c.Read()
	if err != nil {
		log.Fatal("error invalied csv format.", err.Error())
	}
	var rows [][]string
	for {
		row, err := c.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal("error read csv data", err.Error())
		}
		rows = append(rows, row)
	}

	viewer := newCsviwer(column, rows, showColumns, filters, limit)
	viewer.Print()
}

func loadData(path string) (io.Reader, error) {
	if path != "" {
		return os.Open(path)
	}

	stdin := os.Stdin
	stats, err := os.Stdin.Stat()
	if err != nil {
		return nil, err
	}

	if stats.Size() > 0 {
		return stdin, nil
	}

	return nil, fmt.Errorf("data is emptry")
}

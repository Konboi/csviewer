package main

import (
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/olekukonko/tablewriter"
)

type Csviewer struct {
	columns           []string
	showColumns       []string
	showColumunsIndex []int
	rows              [][]string
	rowsMap           []map[string]string
	filters           []string
	funcFilters       map[string]funcFilter
	limit             int
}

type funcFilter func(string) bool

func newCsviwer(columns []string, rows [][]string, showColumns string, filters []string, limit int) *Csviewer {
	show, index := parseShowColumns(columns, showColumns)

	return &Csviewer{
		columns:           columns,
		showColumns:       show,
		showColumunsIndex: index,
		rows:              rows,
		rowsMap:           rowsToMap(columns, rows),
		filters:           filters,
		funcFilters:       getFilters(filters),
		limit:             limit,
	}
}

func rowsToMap(columns []string, rows [][]string) []map[string]string {
	// id,name,email
	// 1, foo, foo@email.com
	// 2, fuga, fuga@email.com
	// ↓
	// {"id": "1", "name", "foo", "email": "foo@email.com"}{"id": "2", "name", "fuga", "email": "fuga@email.com"}
	data := make([]map[string]string, 0, len(rows))
	for _, row := range rows {
		rowMap := make(map[string]string)
		for i, column := range columns {
			rowMap[column] = row[i]
		}
		data = append(data, rowMap)
	}

	return data
}

func parseShowColumns(columns []string, showColumns string) ([]string, []int) {
	index := make([]int, 0)
	show := make([]string, 0)

	if showColumns == "" {
		for i, _ := range columns {
			index = append(index, i)
		}
		return columns, index
	}

	for _, c := range strings.Split(showColumns, ",") {
		for i, column := range columns {
			if c == column {
				show = append(show, column)
				index = append(index, i)
				break
			}
		}
	}

	return show, index
}

func getFilters(filters []string) map[string]funcFilter {
	funcFilters := map[string]funcFilter{}
	for _, f := range filters {
		token := strings.SplitN(f, " ", 3)
		if len(token) < 3 {
			log.Fatalf("filter format is invalid", f)
		}
		key := token[0]
		switch token[1] {
		case ">", ">=", "<=", "<":
			r, err := strconv.ParseFloat(token[2], 64)
			if err != nil {
				log.Fatalf("error parse float: %s \n", token[2])
			}
			funcFilters[key] = func(val string) bool {
				num, err := strconv.ParseFloat(val, 64)
				if err != nil {
					log.Printf("error parse float: %s\n", val)
					return false
				}
				switch token[1] {
				case ">":
					return num > r
				case ">=":
					return num >= r
				case "<=":
					return num <= r
				case "<":
					return num < r
				default:
					return false
				}
			}
		case "==":
			funcFilters[key] = func(val string) bool {
				return val == token[2]
			}
		case "==*":
			funcFilters[key] = func(val string) bool {
				return strings.ToLower(val) == strings.ToLower(token[2])
			}
		case "!=":
			funcFilters[key] = func(val string) bool {
				return val != token[2]
			}
		case "!=*":
			funcFilters[key] = func(val string) bool {
				return strings.ToLower(val) != strings.ToLower(token[2])
			}
		}
	}
	return funcFilters
}

func (v *Csviewer) Print() {
	var printRows [][]string

	count := 0
	for i, rm := range v.rowsMap {
		if v.filter(rm) {
			var row []string
			for _, j := range v.showColumunsIndex {
				row = append(row, v.rows[i][j])
			}
			printRows = append(printRows, row)
			count++
		}

		if 0 < v.limit && v.limit < count {
			break
		}
	}

	t := tablewriter.NewWriter(os.Stdout)
	t.SetHeader(v.showColumns)
	t.AppendBulk(printRows)
	t.Render()
}

func (v *Csviewer) filter(rowMap map[string]string) bool {

	print := true
	for key, funcFilter := range v.funcFilters {
		if !funcFilter(rowMap[key]) {
			print = false
			break
		}
	}

	return print
}
